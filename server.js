if (process.argv.length == 3) {
    process.conf = require("./"+process.argv[2]);
}
else {
    process.conf = require("./conf");
}

var light = require('light');
var urls = require("./urls");

light.LightServer(urls.urls).listen(process.conf.port, '0.0.0.0');
console.log("Server running on port "+process.conf.port+" with db "+process.conf.mongo_url);
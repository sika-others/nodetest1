var db = require("./db");
var request = require('request');
var fs = require('fs');

exports.template = function(path, callback){
    fs.readFile("templates/"+path, function (err, data) {
        if (err) throw err;
        callback(data);
    });
}

exports.log = function(params){
    db.log.insert({
        date: new Date(),
        params: params,
    });
}

exports.get = function (url, callback){
    request(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        callback(body);
      }
    })
}

exports.parseGetParams = function(paramsString){
    params = {}
    paramsString.split("&").forEach(function(item){
        item = item.split("=");
        params[item[0]] = item[1];
    });
    return params;
}

exports.update_servers = function(servers){
    servers.forEach(function(server){
        db.servers.count(servers, function(err, count){
            if (count == 0) db.servers.insert(server);
        });
    });
}
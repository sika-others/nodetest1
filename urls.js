var views = require("./views");
exports.urls = {
    "/": views.home,
    "/add/": views.add,
    "/servers/": views.servers,
    "/update/": views.update,
}
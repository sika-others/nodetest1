var db = require("mongoskin").db(process.conf.mongo_url);

exports.db = db;
exports.servers = db.collection("servers");
exports.log = db.collection("log");

exports.get_servers = function(callback){
    db.collection("servers").find().toArray(function(err, servers){
        callback(servers);
    });
}
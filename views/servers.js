var db = require("../db");
var helpers = require("../helpers");


exports.view = function(req, callback){
    db.get_servers(function(servers){
        callback(JSON.stringify(servers), "application/json");
    });
}
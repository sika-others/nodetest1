var db = require("../db");
var helpers = require("../helpers");


exports.view = function(req, callback){
    console.log(req.url);
    console.log(req.url.indexOf("?"));
    if (req.url.indexOf("?") == -1){
        // form
        helpers.template("add.html", function(html){
            callback(html, "text/html");
        });
    }
    else {
        // process
        server = helpers.parseGetParams(req.url.split("?")[1]);
        db.servers.insert(server);
        callback("ok")
    }
}
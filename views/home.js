var db = require("../db");
var helpers = require("../helpers");


exports.view = function(req, callback){
    params = req.url.split("?")[1] || "";
    helpers.log(params);
    helpers.template("index.html", function(body){
        callback(body, "text/html");
    });
}
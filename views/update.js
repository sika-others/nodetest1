var db = require("../db");
var helpers = require("../helpers");


exports.view = function (req, callback){
    db.get_servers(function(servers){
        servers.forEach(function(server){
            helpers.get("http://"+server.address+":"+server.port+"/servers", function(body){
                servers = JSON.parse(body);
                helpers.update_servers(servers);
            });
        });
    });
    callback();
}